FROM debian:bullseye

# Maintainer
LABEL author="m0rfeo"

# Environment variables
ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Etc/UTC

## Packages
# Add JRE-8U261 source code and extract on /usr/local
ADD ./jre-8u261-linux-x64.tar.gz /usr/local/
# Install firefox and additional required libs
RUN apt-get update -y && apt-get upgrade -y && apt-get install -y firefox-esr libcanberra-gtk-module libcanberra-gtk3-module libpci-dev libegl-dev \ 
#
# Required for X11 and sound support
dbus-x11 pulseaudio \
#
# Install IcedTea package
icedtea-netx
##

# Define WORKDIR
WORKDIR /root

# Volume for WORKDIR
VOLUME ["/root"]

# Instruction running inside container (tail -f RUN forever)
CMD tail -f


