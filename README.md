# Firefox on Docker with JavaWS and Icedtea 

PLEASE READ THIS README!!!

Debian docker container with Firefox, JavaWS and Icedtea installed.

WARNING!! THIS CONTAINER IS USING SOME HOST SOCKETS AS ROOT. YOU ARE NOT ISOLATED FROM YOUR HOST SYSTEM AT ALL, FOR SECURITY REMEMBER TO DELETE THIS CONTAINER AFTER USING IT.

## Specs

- Base Image: Debian:Bullseye
- Jre Version: 8u261
- Firefox package: firefox-esr (latest)
- Icedtea package: icedtea-next (latest)

## Volumes

- ./home:/root

## Expected error

- Some times if you close and open firefox again consecutively the process could crash. Just try to open again or wait some seconds and do it again.

## How to deploy

- Clone this repository (ssh or https). This is https way.

```
git clone https://gitlab.com/m0rfeo/firefox-on-docker-with-javaws-and-icedtea
```

- If you don't want to modify any parameter on docker-compose/Dockerfile just deploy it

```
docker-compose up -d
```

## How to use

- By default this container is named firefox, to run firefox process inside:

```
docker exec -it firefox firefox
```

- To run bash terminal inside firefox container:

```
docker exec -it firefox bash
```

- To delete this container and associated volume:

```
docker container stop firefox && docker container rm firefox && sudo rm REPOSITORYPATH/home -rf 
```

## Misc

Author: m0rfeo.

You can do whatever you want with this proyect except change the license type, i will be grateful if you mention me.

Open to suggestions. :)
